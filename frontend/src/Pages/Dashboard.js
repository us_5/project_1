import React from "react";
import styles from "../styles/Dashboard.module.css";
import bg from "../assets/image 1.png";
import { Link } from "react-router-dom/cjs/react-router-dom.min";

const Dashboard = () => {
  return (
    <div className={styles.container}>
      <img src={bg} alt="Dashboard" className={styles.image} />
      <div className={styles.text}>Welcome To Niranya!</div>
      <div className={styles.buttons}>
        <Link to="/create">
          <button className={`${styles.button} ${styles.button1}`}>
            Create a Poll
          </button>
        </Link>
        <Link to="/vote">
          <button className={`${styles.button} ${styles.button2}`}>
            Cast a Vote
          </button>
        </Link>
      </div>
    </div>
  );
};

export default Dashboard;
