import React from "react";
import logo from "../amico.png";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPaperPlane, faPhone } from '@fortawesome/free-solid-svg-icons';
import { faLinkedin, faInstagram, faFacebook, faTwitterSquare } from '@fortawesome/free-brands-svg-icons';
import style from '../styles/Footer.module.css';
import style3 from '../styles/HomePage.module.css'


const Homepage = () => {
  return (
    <>
    <div className={style3.main}>

    <div style={styles.logoContainer}>
      <div style={styles.text1}>
        <h1 style={{ fontFamily: "Montserrat", fontSize: "50px" }}>
          Empowering <br></br>Secure & Transparent <br></br>Voting for All
        </h1>
      </div>
      <div style={styles.logo1}>
        <img src={logo} alt="Logo" style={styles.logo} />
      </div>
    </div>

     <div className={style3.div4}>
     <footer className={style.footer}>
      {/* This is for the Contact Me */}
      <div>
        <p className={style.contact}>Contact Me</p>
        <div className={style.div}>
          <FontAwesomeIcon icon={faPaperPlane} className={style.icon} />
          <h3 className={style.h1}>bipasamaji73@gmail.com</h3>
        </div>
        <div className={style.div}>
          <FontAwesomeIcon icon={faPhone} className={style.icon} />
          <h3 className={style.h1} >(123) 456-7890</h3> {/* Example phone number */}
        </div>

        <div className={style.socials}>
        
          <FontAwesomeIcon icon={faLinkedin} className={style.icon} />
          <FontAwesomeIcon icon={faInstagram} className={style.icon} />
          <FontAwesomeIcon icon={faFacebook} className={style.icon} />
          <FontAwesomeIcon icon={faTwitterSquare} className={style.icon} />
        </div>

        <button className={style.downloadCvButton}>
          Download CV
        </button>
      </div>
     
      <div className={style.formContainer}>
        <form  className={style.form}>
          <div className={style.formGroup}>
            <label htmlFor="name"></label>
            <input
              type="text"
              id="name"
              name="name"
              placeholder="Your Name"
            />
          </div>
          <div className={style.formGroup}>
            <label htmlFor="email"></label>
            <input
              type="email"
              id="email"
              name="email"
               placeholder="Your Email"

            />
          </div>
          <div className={style.formGroup}>
            <label htmlFor="message"></label>
            <textarea
              id="message"
              name="message"
                placeholder="Your Message"
            />
          </div>
          <button type="submit" className={style.submitButton}>
            Submit
          </button>
        </form>
      </div>
    </footer>
    </div>
  </div>
    </>
  );
};

const styles = {
 
  logoContainer: {
    marginLeft: "10px",
    display: "flex",
    width: "100vw",
    justifyContent: "space-between",
    overflowY: "scroll", // Added overflow for vertical scrolling
  },

  logo: {
    width: "450px",
    height: "520px",
  },
  logo1: {
    display: "flex",
    flex: "1 0 auto",
    justifyContent: "center",
    alignItems: "center",
  },
  text1: {
    display: "flex",
    flex: "1 0 auto",
    justifyContent: "center",
    alignItems: "center",
  },
};

export default Homepage;
