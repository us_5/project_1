import React from "react";
import aboutpng from "../aboutpng.png";
// import Footer from "../components/Footer";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPaperPlane, faPhone } from '@fortawesome/free-solid-svg-icons';
import { faLinkedin, faInstagram, faFacebook, faTwitterSquare } from '@fortawesome/free-brands-svg-icons';
import style from '../styles/Footer.module.css';
// import Menubar from "../components/Menubar";

const About = () => {
  return (
   <>
   {/* <Menubar/> */}
    <div className={style.div2}
      style={{ display: "flex", alignItems: "flex-end", overflowX: "hidden", zIndex: "10" }}
      >
        
      <div style={styles.logoContainer}>
        <div style={{ height: "10vh", marginTop: "5px" }}>
          <h1 style={{ color: "Black" }}>ABOUT</h1>
        </div>
        <div style={styles.section2}>
          <div style={styles.text1}>
            <h3
              style={{
                fontFamily: "Montserrat",
                fontSize: "25px",
                textAlign: "left",
              }}
              >
              What is NIRNAYA 
            </h3>
            <h3
              style={{
                fontFamily: "Montserrat",
                fontSize: "15px",
                width: "25vw",
                textAlign: "left",
              }}
              >
              Nirnaya is a cutting-edge voting system designed to ensure the
              integrity and transparency of the electoral process. By leveraging
              advanced security protocols, Nirnaya guarantees that every vote is
              accurately recorded and securely stored. Our user-friendly
              interface makes participation straightforward, ensuring that
              everyone's voice is heard with confidence. Experience a new era of
              voting where trust and clarity are at the forefront.
            </h3>
          </div>
          <div style={styles.logo1}>
            <img src={aboutpng} alt="Logo" style={styles.logo} />
          </div>
        </div>
      </div>
      <footer className={style.footer}>
      {/* This is for the Contact Me */}
      <div>
        <p className={style.contact}>Contact Me</p>
        <div className={style.div}>
          <FontAwesomeIcon icon={faPaperPlane} className={style.icon} />
          <h3 className={style.h1}>bipasamaji73@gmail.com</h3>
        </div>
        <div className={style.div}>
          <FontAwesomeIcon icon={faPhone} className={style.icon} />
          <h3 className={style.h1} >(123) 456-7890</h3> {/* Example phone number */}
        </div>

        <div className={style.socials}>
          {/* Add social media icons here */}
          <FontAwesomeIcon icon={faLinkedin} className={style.icon} />
          <FontAwesomeIcon icon={faInstagram} className={style.icon} />
          <FontAwesomeIcon icon={faFacebook} className={style.icon} />
          <FontAwesomeIcon icon={faTwitterSquare} className={style.icon} />
        </div>

        <button className={style.downloadCvButton}>
          Download CV
        </button>
      </div>

     
      <div className={style.formContainer}>
        <form  className={style.form}>
          <div className={style.formGroup}>
            <label htmlFor="name"></label>
            <input
              type="text"
              id="name"
              name="name"
              placeholder="Your Name"
              />
          </div>
          <div className={style.formGroup}>
            <label htmlFor="email"></label>
            <input
              type="email"
              id="email"
              name="email"
              placeholder="Your Email"
              
              />
          </div>
          <div className={style.formGroup}>
            <label htmlFor="message"></label>
            <textarea
              id="message"
              name="message"
              placeholder="Your Message"
              />
          </div>
          <button type="submit" className={style.submitButton}>
            Submit
          </button>
        </form>
      </div>
    </footer>
    </div>

</>
  );
};

const styles = {
  logoContainer: {
    background: "white",
    display: "flex",
    flexDirection: "column",
    width: "100vw",
    justifyContent: "space-between",
    height: "65vh",
    borderTopLeftRadius: "50%",
    borderTopRightRadius: "50%",
    scale: "1.5",
    alignItems: "center",
    color: "white",
    textAlign: "center",
    paddingTop: "120px",
  },
  section2: {
    display: "flex",
    alignItems: "flex-start",
    height: "100vh",
    width: "63vw",
  },
  logo: {
    width: "350px",
    height: "320px",
  },

  logo1: {
    display: "flex",
    flex: "1",
    justifyContent: "center",
    alignItems: "center",
  },
  text1: {
    display: "flex",
    flexDirection: "Column",
    color: "#480C77",
    flex: "1",
    marginLeft: "20px",
  },
};

export default About;
