import React from 'react';
import styles from '../styles/Team.module.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPaperPlane, faPhone } from '@fortawesome/free-solid-svg-icons';
import { faLinkedin, faInstagram, faFacebook, faTwitterSquare } from '@fortawesome/free-brands-svg-icons';
import style from '../styles/Footer.module.css';

// Import images
import member1 from '../assets/WhatsApp Image 2024-07-18 at 15.47.04_2186ca0f 1.png';
import member2 from '../assets/WhatsApp Image 2024-07-18 at 15.47.04_2ef5d36f 1.png';
import member3 from '../assets/WhatsApp Image 2024-07-18 at 15.47.05_26a2fae0 1.png';
import member4 from '../assets/WhatsApp Image 2024-07-18 at 15.47.05_7767ecdb 1.png';
import member5 from '../assets/WhatsApp Image 2024-07-18 at 15.47.57_a22228e9 1.png';

const Team = () => {
    return (
        <>

      <div className={styles.pageContainer}>
        <h1 className={styles.heading}>THE TEAM</h1>
        <div className={styles.imageContainer}>
          <img src={member1} alt="Team Member 1" className={`${styles.image} ${styles.image1}`} />
          <img src={member5} alt="Team Member 2" className={`${styles.image} ${styles.image2}`} />
          <img src={member3} alt="Team Member 3" className={`${styles.image} ${styles.image3}`} />
        </div>
        <div className={styles.imageContainer}>
          <img src={member4} alt="Team Member 4" className={`${styles.image} ${styles.image4}`} />
          <img src={member2} alt="Team Member 5" className={`${styles.image} ${styles.image5}`} />
        </div>
        <div className={styles.div2}>
        <footer className={style.footer}>
      {/* This is for the Contact Me */}
      <div>
        <p className={style.contact}>Contact Me</p>
        <div className={style.div}>
          <FontAwesomeIcon icon={faPaperPlane} className={style.icon} />
          <h3 className={style.h1}>bipasamaji73@gmail.com</h3>
        </div>
        <div className={style.div}>
          <FontAwesomeIcon icon={faPhone} className={style.icon} />
          <h3 className={style.h1} >(123) 456-7890</h3> {/* Example phone number */}
        </div>

        <div className={style.socials}>
          {/* Add social media icons here */}
          <FontAwesomeIcon icon={faLinkedin} className={style.icon} />
          <FontAwesomeIcon icon={faInstagram} className={style.icon} />
          <FontAwesomeIcon icon={faFacebook} className={style.icon} />
          <FontAwesomeIcon icon={faTwitterSquare} className={style.icon} />
        </div>

        {/* <button className={style.downloadCvButton}>
          Download CV
        </button> */}
      </div>

      <div className={styles.formContainer2}>
        <form  className={style.form}>
          <div className={style.formGroup}>
            <label htmlFor="name"></label>
            <input
              type="text"
              id="name"
              name="name"
              placeholder="Your Name"
            />
          </div>
          <div className={style.formGroup}>
            <label htmlFor="email"></label>
            <input
              type="email"
              id="email"
              name="email"
               placeholder="Your Email"

            />
          </div>
          <div className={style.formGroup}>
            <label htmlFor="message"></label>
            <textarea
              id="message"
              name="message"
                placeholder="Your Message"
            />
          </div>
          <button type="submit" className={style.submitButton}>
            Submit
          </button>
        </form>
      </div>
    </footer>

        </div>
      </div>
      
        </>
    );
};

export default Team;
