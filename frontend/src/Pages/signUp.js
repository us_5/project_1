import React from 'react';
import styles from '../styles/signUp.module.css';
import { Link } from 'react-router-dom';

export const signUp = () => {
  return (
    <div className={styles.container}>
      <div className={styles.navbar}>
        <div className={styles.formContainer}>
          <h2 className={styles.title}>SIGN UP</h2>
          <form className={styles.form}>
            <div className={styles.formGroup}>
              <label htmlFor="id"></label>
              <input type="text" id="id" name="id" placeholder="Enter your ID" />
            </div>
            <div className={styles.formGroup}>
              <label htmlFor="username"></label>
              <input type="text" id="username" name="username" placeholder="Enter your username" />
            </div>
            <div className={styles.formGroup}>
              <label htmlFor="password"></label>
              <input type="password" id="password" name="password" placeholder="Enter your password" />
            </div>
            <div className={styles.btn}>
              <Link to="/Dashboard" className={styles.submitButton}>
                Sign Up
              </Link>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};
