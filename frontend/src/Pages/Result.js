import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import styles from '../styles/Result.module.css'; // Import the CSS module

const ResultsPage = () => {
  const { pollId } = useParams(); // Get pollId from URL params
  const [pollData, setPollData] = useState(null);
  const [selectedVote, setSelectedVote] = useState(null); // Track selected vote
  const [isSubmitted, setIsSubmitted] = useState(false); // Track if vote has been submitted

  useEffect(() => {
    // Fetch or filter data based on pollId
    const fetchData = () => {
      // Load data from local storage based on pollId
      const savedQuestions = localStorage.getItem(`poll_${pollId}`);
      if (savedQuestions) {
        const data = JSON.parse(savedQuestions);
        setPollData(data);
      } else {
        // Handle the case where no data is found
        setPollData([]);
      }
    };

    fetchData();
  }, [pollId]);

  const handleVoteChange = (questionId) => {
    setSelectedVote(questionId); // Set selected vote based on questionId
  };

  const handleSubmitVote = () => {
    if (selectedVote === null) return; // Do nothing if no vote selected

    // Save vote in local storage or send to backend
    localStorage.setItem(`vote_${pollId}`, JSON.stringify({ questionId: selectedVote }));

    setIsSubmitted(true); // Mark as submitted
  };

  if (!pollData) return <div>Loading...</div>;

  return (
    <div className={styles.resultsPage}>
      <h1>Results for Poll ID: {pollId}</h1>
      {pollData.length === 0 ? (
        <p>No questions found for this poll.</p>
      ) : (
        <div className={styles.pollResults}>
          {pollData.map(question => (
            <div key={question.id} className={styles.question}>
              <h2>{question.text}</h2>
              <p>Created on: {question.createdOn}</p>
              <input
                type="radio"
                id={`question-${question.id}`}
                name="vote"
                value={question.id}
                checked={selectedVote === question.id}
                onChange={() => handleVoteChange(question.id)}
              />
              <label htmlFor={`question-${question.id}`}>Vote for this question</label>
            </div>
          ))}
          <button
            className={styles.submitButton}
            onClick={handleSubmitVote}
            disabled={isSubmitted}
          >
            {isSubmitted ? 'Vote Submitted' : 'Submit Vote'}
          </button>
        </div>
      )}
    </div>
  );
};

export default ResultsPage;
