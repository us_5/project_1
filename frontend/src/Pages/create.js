import React, { useState } from 'react';
import styles from '../styles/create.module.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChartSimple } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';

const Create = () => {
  const [question, setQuestion] = useState('');
  const [options, setOptions] = useState(['', '', '', '']);
  const [isCreated, setIsCreated] = useState(false);
  const [selectedType, setSelectedType] = useState('Option 1');
  const [pollId, setPollId] = useState(null); // State for Poll ID

  const handleQuestionChange = (e) => {
    setQuestion(e.target.value);
  };

  const handleOptionChange = (index, e) => {
    const newOptions = [...options];
    newOptions[index] = e.target.value;
    setOptions(newOptions);
  };

  const handleCreateClick = () => {
    setIsCreated(true);
    setPollId(`Poll-${Date.now()}`); // Generate a unique Poll ID
  };

  const handleSelectChange = (e) => {
    setSelectedType(e.target.value);
  };

  return (
    <div className={styles.div}>
      <nav className={styles.navbar}>
        <ul className={styles.navList}>
          <div className={styles.flexing}>
            <Link to="/create">
              <div>
                <h4 className={styles.h1}>New Poll</h4>
              </div>
            </Link>
            <Link to="/vote">
              <div>
                <h4 className={styles.h1}>Go To Vote</h4>
              </div>
            </Link>
            <Link to="/create">
              <div>
                <h4 className={styles.h1}>Create Poll</h4>
              </div>
            </Link>
            <Link to="">
              <div>
                <h4 className={styles.h1}>Share</h4>
              </div>
            </Link>
          </div>
        </ul>
      </nav>
      <div className={styles.container}>
        <div className={styles.divison}>
          <div className={styles.buttonContainer1}>
            <button className={styles.newPollButtonPlus1}>
              New Poll+
            </button>
          </div>
          <div className={styles.buttonContainer2}>
            <button className={styles.newPollButton2}>
              <FontAwesomeIcon icon={faChartSimple} className={styles.iconStyle} />
              New Poll
            </button>
          </div>
        </div>
        <div className={styles.card}>
          <h2>{isCreated ? question : 'Your Poll Question Here'}</h2>
          {isCreated && (
            <div className={styles.optionsContainer}>
              {options.map((option, index) => (
                <button key={index} className={styles.optionButton}>
                  {option}
                </button>
              ))}
            </div>
          )}
          {/* Display Poll ID here */}
          {isCreated && pollId && (
            <div className={styles.pollIdContainer}>
              <p className={styles.pollId}>Poll ID: {pollId}</p>
            </div>
          )}
        </div>
        <div className={styles.sidebar}>
          <div className={styles.container1}>Content</div>
          <div className={styles.container2}>
            <label htmlFor="slide-type">Slide Type:</label>
            <select
              id="slide-type"
              className={styles.dropdown}
              value={selectedType}
              onChange={handleSelectChange}
            >
              <option value="Option 1">Multiple Choice Question</option>
              <option value="Option 2">Multiple Choice Question</option>
              <option value="Option 3">Multiple Choice Question</option>
            </select>
          </div>
          <hr />
          <h3 className={styles.heading}>Questions</h3>
          <input
            type="text"
            placeholder="Enter your question"
            value={question}
            onChange={handleQuestionChange}
          />
          <h3 className={styles.heading}>Options:</h3>
          {options.map((option, index) => (
            <div key={index} className={styles.inputWrapper}>
              <input
                type="text"
                placeholder={`Option ${index + 1}`}
                value={option}
                onChange={(e) => handleOptionChange(index, e)}
              />
              {/* <div className={styles.colorIndicator}></div> */}
            </div>
          ))}
          <button onClick={handleCreateClick}>
            {isCreated ? 'Created' : 'Create'}
          </button>
        </div>
      </div>
    </div>
  );
};

export default Create;
