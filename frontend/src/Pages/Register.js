import React from "react";
import registerpng from "../registerpng.png";
import { Link } from "react-router-dom";

const Register = () => {
  return (
    <div style={styles.logoContainer}>
      <div style={styles.text1}>
        <h3
          style={{
            fontFamily: "Montserrat",
            fontSize: "45px",
            textAlign: "left",
          }}
        >
          REGISTER NOW !
        </h3>
        <h3
          style={{
            fontFamily: "Montserrat",
            fontSize: "25px",
            textAlign: "left",
          }}
        >
          If you already have an account, then what are you waiting for? Log in
          to your account and explore our newest features..
        </h3>
        <div style={{ display: "flex" }}>
        <Link to="/signUp" style={styles.link}>
            <button type="button" style={styles.button1}>
            Sign Up
            </button>
        </Link>
        <Link to="/signIn" style={styles.link}>
            <button type="button" style={styles.button1}>
            Log In
            </button>
        </Link>
        </div>
      </div>
      <div style={styles.logo1}>
        <img src={registerpng} alt="Logo" style={styles.logo} />
      </div>
    </div>
  );
};

const styles = {
  logoContainer: {
    marginLeft: "10px",
    display: "flex",
    width: "100vw",
    justifyContent: "space-between",
  },
  logo: {
    width: "450px",
    height: "500px",
  },
  logo1: {
    display: "flex",
    flex: "1",
    justifyContent: "center",
    alignItems: "center",
    marginRight: "60px",
  },
  text1: {
    marginLeft: "60px",
    display: "flex",
    flexDirection: "Column",
    color: "white",
    flex: "1",
    justifyContent: "center",
    alignItems: "center",
  },
  button1: {
    width: "150px",
    color: "white",
    fontWeight: "bold",
    marginLeft: "50px",
    backgroud: "none",
    padding: "15px 10px",
    fontSize: "25px",
    borderRadius: "50px",
    outline: "none",
    backgroundColor: "transparent",
    borderColor: "white",
    borderWidth: "3px",
  },
};

export default Register;
