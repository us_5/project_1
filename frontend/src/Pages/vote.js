import React, { useState } from 'react';
import styles from '../styles/vote.module.css'; // Import the CSS module
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faUser, faTrashAlt } from '@fortawesome/free-solid-svg-icons'; // Import the icons
import { Link } from 'react-router-dom'; // Import Link for navigation

const Vote = () => {
  const [pollId, setPollId] = useState('');
  const [newQuestion, setNewQuestion] = useState('');
  const [questions, setQuestions] = useState(() => {
    // Load existing questions from local storage if available
    const savedQuestions = localStorage.getItem(`poll_${pollId}`);
    return savedQuestions ? JSON.parse(savedQuestions) : [];
  });

  const handlePollIdChange = (e) => {
    setPollId(e.target.value);
  };

  const handleQuestionChange = (e) => {
    setNewQuestion(e.target.value);
  };

  const handleAddQuestion = () => {
    if (newQuestion.trim() === '') return; // Do nothing if input is empty

    const currentDate = new Date().toISOString().split('T')[0]; // Format: YYYY-MM-DD
    const newQuestionObj = {
      id: Date.now(), // Unique id based on timestamp
      text: newQuestion,
      createdOn: currentDate
    };

    const updatedQuestions = [...questions, newQuestionObj];
    setQuestions(updatedQuestions);
    setNewQuestion(''); // Clear input field

    // Save to local storage
    localStorage.setItem(`poll_${pollId}`, JSON.stringify(updatedQuestions));
  };

  const handleDeleteQuestion = (id) => {
    const updatedQuestions = questions.filter(question => question.id !== id);
    setQuestions(updatedQuestions);
    localStorage.setItem(`poll_${pollId}`, JSON.stringify(updatedQuestions));
  };

  const handleQuestionClick = (questionId) => {
    // Open a new window with question details
    window.open(`/details/${questionId}`, '_blank');
  };

  return (
    <div className={styles.whiteBackground}>
      <nav className={styles.navbar}>
        <div className={styles.flexing}>
          <div>
            <FontAwesomeIcon icon={faBars} className={styles.icon} />
          </div>
          <div className={styles.h1}>NIRNAYA</div>
          <div>
            <FontAwesomeIcon icon={faUser} className={styles.icon} />
          </div>
        </div>
      </nav>

      <div className={styles.inputSection}>
        <div className={styles.input2}>
          <label htmlFor="poll-id" className={styles.label}>
            Enter the Poll Id:
          </label>
          <input
            type="text"
            id="poll-id"
            className={styles.input}
            placeholder=""
            value={pollId}
            onChange={handlePollIdChange}
          />
        </div>

        <h2 className={styles.activePollsHeading}>Active Polls</h2>

        <label htmlFor="question-input" className={styles.label}>
          Enter a Question:
        </label>
        <input
          type="text"
          id="question-input"
          className={styles.input}
          placeholder="Enter your question"
          value={newQuestion}
          onChange={handleQuestionChange}
        />
        <button onClick={handleAddQuestion} className={styles.addButton}>
          Add Question
        </button>
      </div>

      <div className={styles.questionsContainer}>
        {questions.map(question => (
          <div key={question.id} className={styles.question} onClick={() => handleQuestionClick(question.id)}>
            <span>{question.text}</span>
            <span className={styles.createdOn}>Created on: {question.createdOn}</span>
            <button 
              className={styles.deleteButton} 
              onClick={(e) => {
                e.stopPropagation(); // Prevent triggering question click event
                handleDeleteQuestion(question.id);
              }}
            >
              <FontAwesomeIcon icon={faTrashAlt} />
            </button>
            <Link to={`/results/${pollId}`} className={styles.resultsLink}>
              View Results
            </Link>
          </div>
        ))}
      </div>
    </div>
  );
}

export default Vote;
