import React from 'react';
import styles from '../styles/signUp.module.css'; // Make sure the path is correct
import { Link } from 'react-router-dom';

const signIn = () => {
  return (
    <div className={styles.container}>
      <div className={styles.navbar}>
        <div className={styles.formContainer}>
          <h2 className={styles.title}>LOG IN</h2>
          <form className={styles.form}>
            <div className={styles.formGroup}>
              <label htmlFor="id">ID</label>
              <input type="text" id="id" name="id" placeholder="Enter your ID" />
            </div>
            <div className={styles.formGroup}>
              <label htmlFor="username">Username</label>
              <input type="text" id="username" name="username" placeholder="Enter your username" />
            </div>
            <div className={styles.formGroup}>
              <label htmlFor="password">Password</label>
              <input type="password" id="password" name="password" placeholder="Enter your password" />
            </div>
            <div className={styles.btn}>
              <Link to="/Dashboard" className={styles.submitButton}>
                Log In
              </Link>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default signIn;
