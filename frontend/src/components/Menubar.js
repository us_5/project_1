import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

const Menubar = () => {
  const [scrolled, setScrolled] = useState(false);

  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY >0) { // Adjust the scroll threshold as needed
        setScrolled(true);
      } else {
        setScrolled(false);
      }
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <nav style={{ ...styles.nav, background: scrolled ? 'black' : 'linear-gradient(to bottom, #000000 0.1%, #480c77 100%)' }}>
      <div style={styles.logoContainer}>
        <h1 style={styles.logoText}>
          NIRNAYA
        </h1>
      </div>
      <ul style={styles.ul}>
        <li style={styles.li}>
          <Link to="/" style={styles.link}>
            Home
          </Link>
        </li>
        <li style={styles.li}>
          <Link to="/about" style={styles.link}>
            About
          </Link>
        </li>
        <li style={styles.li}>
          <Link to="/contact" style={styles.link}>
            Contact
          </Link>
        </li>
        <li style={styles.li}>
          <Link to="/team" style={styles.link}>
            Team
          </Link>
        </li>
        <li id="register" style={styles.li}>
          <Link to="/register" style={styles.link}>
            REGISTER
          </Link>
        </li>
      </ul>
    </nav>
  );
};

const styles = {
  nav: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: "0px 70px", // Set padding to zero
    width: "100%",
    boxSizing: "border-box",
    height: "100px",
    overflowY: "hidden", // Add this line to prevent overflow
    zIndex: 5, // Ensure it's on top
    // transition: "background-color 0.3s ease", // Smooth transition for background color
  },
  logoContainer: {
    // Add any specific styles for the logo container if needed
  },
  logoText: {
    color: "white", // White text color for the logo
    fontSize: "50px",
    fontFamily: "Belleza",
  },
  ul: {
    listStyleType: "none",
    padding: 0,
    display: "flex",
    justifyContent: "flex-end",
    flex: "1 1 auto",
  },
  li: {
    marginLeft: "30px",
  },
  link: {
    color: "white", // White text color for links
    textDecoration: "none",
    fontSize: "20px",
  },
};

export default Menubar;
