import "./App.css";
import { Route } from "react-router-dom";
import Homepage from "./Pages/Homepage";
import About from "./Pages/About";
import Help from "./Pages/Help";
import Register from "./Pages/Register";
import Team from "./Pages/Team";
import signIn from './Pages/signIn';
import { signUp } from "./Pages/signUp";
import Dashboard from "./Pages/Dashboard";
import create from "./Pages/create";
import vote from "./Pages/vote";
import ResultsPage from "./Pages/Result";

function App() {
  return (
    <div className="App">
      <Route path="/" component={Homepage} exact />
      <Route path="/about" component={About} />
      <Route path="/help" component={Help} />
      <Route path="/register" component={Register} />
      <Route path="/Team" component={Team} />
      <Route path="/signUp" component={signUp} />
      <Route path="/signIn" component={signIn} />
      <Route path="/Dashboard" component={Dashboard} />
      <Route path="/create" component={create} />
      <Route path="/vote" component={vote} />
      <Route path="/result/:pollId" component={ResultsPage} />
    </div>
  );
}

export default App;
