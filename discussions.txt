TEAM MEMBERS:  Adyasha Das
               Yashaswini Sharma
               Lithikha B
               Bipasa Maji
               Sanjana S Pillai

MEETING 1: 25/06/2024

We had an online meeting to discuss about what project we should do and following were the key points covered.

Idea 1: Game using PyGame
Idea 2: Dehaze backgrounds in photos using Machine Learning
Idea 3: An AI which finds the changes and writes the commits in GITLAB and changes README
Idea 4: Webscrapping - for free resources to read books online
Idea 5: Transforming meetings into actionable insignts 
Idea 6: Pictorial representation of data
Idea 7: Instagram spam detection plugin
Idea 8: Snake Game
Idea 9: Group shuffler for students to work in groups
Idea 10: Attendance tracker which reminds students to mark Attendance
Idea 11: Generate time table as per user's daily activities and remind them continuously 
Idea 12: Git-based Collaboration Platform: Develop a platform similar to GitHub or GitLab 
         that focuses on specific features such as code review, issue tracking, wiki documentation, 
         and team collaboration. Use technologies like Django (Python), Rails (Ruby), or Node.js.


Interested Genres: Photography, Gaming, Food, Reading

After discussions, we decided to pick the top :3 ideas from this, discuss with aruvi and then move on.

MEETING 2: 26/06/2024

We spoke to aruvi and asokan regarding our ideas. The top 3 ideas which we discussed were:
    1) Snake Game and ludo
    2) Pictorial representation of data
    3) An AI which finds the changes and writes the commits in GITLAB and changes README

Aruvi and asokan approved idea (2) but we felt we should do something better.
We came up with few more ideas - 1) Personalised schedule maker 
                                 2) Blockchain voting

Asokan asked us to go on with online voting system using Blockchain

so we started working on it 

yashaswini and adyasha made the latex presentation
lithikha, adyasha, bipasa were researching on the different types on voting systems and which type would suit our approach
sanjana was searching for research papers that have been released in this genre.

After complete research we decided that the system will focus on "first past the post" voting system which is being followed in idnia and many other countries

UPDATES:
Yashaswini started learning Docker
Adyasha and Bipasa are working on how to implement the approach
Lithika and Yashaswini created a graphViz plot to show the backend and frontend work


The Latex dec was presented and we found a lot of errors in the presentation. Aruvi asked us to show the working of auditability feature of our system in 2 weeks, that is how fake votes are recognised and ignored and votes are stored.


MEETING 3: 26/06/2024 (ONLINE)

We decided to modify the graphViz flowchart a little bit. Work can be started only if we learn blockchain and go, so we decided to start learning that. Adyasha told she'll speak to her college senior about learning blockchain and even sanjana has asked her college staff. 

Since the LaTeX deck had errors, modification was needed, which yashaswini told she'll rectify.

For now weve decided to make a basic model, with a smaller dataset.
                                  
27/06/2024

Finally after 7 presentations we got our idea approved. Now we will start working on the project


03/07/2024

Yashaswini had completed the work for user registration and authentication.
She also instructed the others on how to install hyperledger fabric.

05/07/2024

There were many errors which we were facing while working on the backend since there wasnt much documentation regarding hyperledger fabric and go
The front end part also will be started

09/07/2024

We were supposed to have  our first project progress meeting with aruvi where we showed her little bit of our backend code and front end which we had completed
40% of the work had been completed by 10 July
Our project is currently at the stage where we have been able to create a database and commands for authenticating and creating a user but when we try to automate the process(just run a script and not have to initialize each environment variable and command manually), we are running into trouble.

10/07/2024

We completed the progress review and got a 2/10 for the work we completed.

12/07/2024

work completed : front end - 80%
                 back end - on process

work to be done : minor debugging and integration of front end and backend

15/07/2024

the chaincode is working and docker network also is working
all errors in the backend have been rectified 
work related to vote casting has begun
the voting chaincode along with the commands that need to be executed for the file to be packaged, deployed and used have been added.

17/07/2024

User authentication has been successfully completed.
front end and back end have to be integrated with each other

18/07/2024

A figma model of how the webiste will look is going to be created with a polls page, sign in sign up, home page, a notice page where ongoing campaigns will be announced.
the authentication is being changed so as to use a public key and sign to login for added security.

22/07/2024

Button for vote casting is created. Backend API Integration has been done. The enhanced version of the homepage has been completed with figma. The authentication cookies error is solved.


25/07/2024

Major changes had to be made during the integration of backend and frontend.
