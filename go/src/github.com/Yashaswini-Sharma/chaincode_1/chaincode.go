package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

type SmartContract struct {
	contractapi.Contract
}

type User struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	Password string `json:"password"`
}

// InitLedger adds a base set of users to the ledger
func (s *SmartContract) InitLedger(ctx contractapi.TransactionContextInterface) error {
	users := []User{
		{ID: "user1", Name: "Alice", Password: "password1"},
		{ID: "user2", Name: "Bob", Password: "password2"},
	}

	for i, user := range users {
		userAsBytes, _ := json.Marshal(user)
		err := ctx.GetStub().PutState("USER"+strconv.Itoa(i), userAsBytes)
		if err != nil {
			return fmt.Errorf("failed to put user to world state. %s", err.Error())
		}
	}

	return nil
}

// RegisterUser adds a new user to the world state with given details
func (s *SmartContract) RegisterUser(ctx contractapi.TransactionContextInterface, id string, name string, password string) error {
	user := User{
		ID:       id,
		Name:     name,
		Password: password,
	}

	userAsBytes, _ := json.Marshal(user)
	return ctx.GetStub().PutState(id, userAsBytes)
}

// AuthenticateUser checks if the user credentials are correct
func (s *SmartContract) AuthenticateUser(ctx contractapi.TransactionContextInterface, id string, password string) (bool, error) {
	userAsBytes, err := ctx.GetStub().GetState(id)
	if err != nil {
		return false, fmt.Errorf("failed to read from world state. %s", err.Error())
	}
	if userAsBytes == nil {
		return false, fmt.Errorf("user not found")
	}

	user := new(User)
	_ = json.Unmarshal(userAsBytes, user)

	if user.Password != password {
		return false, fmt.Errorf("incorrect password")
	}

	return true, nil
}

func main() {
	chaincode, err := contractapi.NewChaincode(new(SmartContract))
	if err != nil {
		fmt.Printf("Error create fabvote chaincode: %s", err.Error())
		return
	}

	if err := chaincode.Start(); err != nil {
		fmt.Printf("Error starting fabvote chaincode: %s", err.Error())
	}
}

