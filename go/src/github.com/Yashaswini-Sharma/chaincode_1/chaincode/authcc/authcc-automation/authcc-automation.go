package main

import (
	"log"
	"path/filepath"

	"github.com/hyperledger/fabric-sdk-go/pkg/client/resmgmt"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/providers/fab"
	"github.com/hyperledger/fabric-sdk-go/pkg/core/config"
	"github.com/hyperledger/fabric-sdk-go/pkg/fabsdk"
)

func main() {
	// Load the SDK config
	sdk, err := fabsdk.New(config.FromFile("/home/naina/Projects/project_1/go/src/github.com/Yashaswini-Sharma/fabric-samples/test-network/config.yaml"))
	if err != nil {
		log.Fatalf("Failed to create new SDK: %s", err)
	}
	defer sdk.Close()

	// Create a new resource management client for Org1
	org1AdminContext := sdk.Context(fabsdk.WithUser("Admin"), fabsdk.WithOrg("Org1"))
	resMgmtClient, err := resmgmt.New(org1AdminContext)
	if err != nil {
		log.Fatalf("Failed to create new resource management client: %s", err)
	}

	// Package chaincode
	ccPkg, err := resmgmtClient.LifecyclePackageCC("authcc", resmgmt.WithPath(filepath.Join("..", "go", "authcc")))
	if err != nil {
		log.Fatalf("Failed to package chaincode: %s", err)
	}

	// Install chaincode on peer0.org1
	installCCReq := resmgmt.LifecycleInstallCCRequest{Name: "authcc", Package: ccPkg}
	_, err = resMgmtClient.LifecycleInstallCC(installCCReq)
	if err != nil {
		log.Fatalf("Failed to install chaincode on peer0.org1: %s", err)
	}

	// Install chaincode on peer0.org2
	org2AdminContext := sdk.Context(fabsdk.WithUser("Admin"), fabsdk.WithOrg("Org2"))
	resMgmtClientOrg2, err := resmgmt.New(org2AdminContext)
	if err != nil {
		log.Fatalf("Failed to create new resource management client for Org2: %s", err)
	}
	_, err = resMgmtClientOrg2.LifecycleInstallCC(installCCReq)
	if err != nil {
		log.Fatalf("Failed to install chaincode on peer0.org2: %s", err)
	}

	// Approve chaincode definition for Org1
	approveCCReq := resmgmt.LifecycleApproveCCRequest{
		Name:      "authcc",
		Version:   "1.0",
		PackageID: ccPkg.PackageID,
		Sequence:  1,
		Policy:    fab.Policy("OR('Org1MSP.member','Org2MSP.member')"),
	}
	err = resMgmtClient.LifecycleApproveCC("mychannel", approveCCReq)
	if err != nil {
		log.Fatalf("Failed to approve chaincode definition for Org1: %s", err)
	}

	// Approve chaincode definition for Org2
	err = resMgmtClientOrg2.LifecycleApproveCC("mychannel", approveCCReq)
	if err != nil {
		log.Fatalf("Failed to approve chaincode definition for Org2: %s", err)
	}

	// Commit chaincode definition
	commitCCReq := resmgmt.LifecycleCommitCCRequest{
		Name:         "authcc",
		Version:      "1.0",
		Sequence:     1,
		Policy:       fab.Policy("OR('Org1MSP.member','Org2MSP.member')"),
		InitRequired: true,
	}
	err = resMgmtClient.LifecycleCommitCC("mychannel", commitCCReq)
	if err != nil {
		log.Fatalf("Failed to commit chaincode definition: %s", err)
	}

	log.Println("Chaincode 'authcc' installed, approved, and committed successfully.")
}
