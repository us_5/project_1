package main

import (
    "encoding/json"
    "fmt"
    "log"

    "github.com/hyperledger/fabric-contract-api-go/contractapi"
)

type SmartContract struct {
    contractapi.Contract
}

type User struct {
    ID       string `json:"id"`
    Username string `json:"username"`
    Password string `json:"password"`
}

func (s *SmartContract) CreateUser(ctx contractapi.TransactionContextInterface, id string, username string, password string) error {
    user := User{
        ID:       id,
        Username: username,
        Password: password,
    }

    userAsBytes, _ := json.Marshal(user)
    return ctx.GetStub().PutState(id, userAsBytes)
}

func (s *SmartContract) AuthenticateUser(ctx contractapi.TransactionContextInterface, username string, password string) (bool, error) {
    queryString := fmt.Sprintf("{\"selector\":{\"username\":\"%s\",\"password\":\"%s\"}}", username, password)
    resultsIterator, err := ctx.GetStub().GetQueryResult(queryString)
    if err != nil {
        return false, err
    }
    defer resultsIterator.Close()

    if resultsIterator.HasNext() {
        return true, nil
    }

    return false, nil
}

func main() {
    chaincode, err := contractapi.NewChaincode(new(SmartContract))
    if err != nil {
        log.Panicf("Error creating chaincode: %v", err)
    }

    if err := chaincode.Start(); err != nil {
        log.Panicf("Error starting chaincode: %v", err)
    }
}

