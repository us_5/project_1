package main

import (
	"encoding/json"
	"fmt"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

// Poll describes basic details of a poll
type Poll struct {
	ID         string          `json:"id"`
	Title      string          `json:"title"`
	Candidates []string        `json:"candidates"`
	Votes      map[string]int  `json:"votes"`
	Voters     map[string]bool `json:"voters"`
}

// CreatePoll initializes a new poll
func (s *SmartContract) CreatePoll(ctx contractapi.TransactionContextInterface, title string, candidates []string) (string, error) {
	// Input validation
	if title == "" || len(candidates) == 0 {
		return "", fmt.Errorf("invalid input, all fields are required")
	}

	pollID := generateID("POLL")
	poll := Poll{
		ID:         pollID,
		Title:      title,
		Candidates: candidates,
		Votes:      make(map[string]int),
		Voters:     make(map[string]bool),
	}

	for _, candidate := range candidates {
		poll.Votes[candidate] = 0
	}

	pollAsBytes, err := json.Marshal(poll)
	if err != nil {
		return "", fmt.Errorf("failed to marshal poll: %v", err)
	}

	err = ctx.GetStub().PutState(pollID, pollAsBytes)
	if err != nil {
		return "", fmt.Errorf("failed to put poll in world state: %v", err)
	}

	// Emitting an event
	err = ctx.GetStub().SetEvent("CreatePoll", pollAsBytes)
	if err != nil {
		return "", fmt.Errorf("failed to set event for poll creation: %v", err)
	}

	return pollID, nil
}

// CastVote allows a user to cast a vote
// CastVote allows a user to cast a vote
func (s *SmartContract) CastVote(ctx contractapi.TransactionContextInterface, pollID string, candidate string, voterID string) error {
	// Check if the voter exists in the database
	voterAsBytes, err := ctx.GetStub().GetState(voterID)
	if err != nil {
		return fmt.Errorf("failed to read voter from world state: %v", err)
	}
	if voterAsBytes == nil {
		return fmt.Errorf("voter %s does not exist", voterID)
	}

	// Get the poll from state
	pollAsBytes, err := ctx.GetStub().GetState(pollID)
	if err != nil {
		return fmt.Errorf("failed to read poll from world state: %v", err)
	}
	if pollAsBytes == nil {
		return fmt.Errorf("poll %s does not exist", pollID)
	}

	// Unmarshal the poll
	var poll Poll
	err = json.Unmarshal(pollAsBytes, &poll)
	if err != nil {
		return fmt.Errorf("failed to unmarshal poll: %v", err)
	}

	// Check if the voter has already voted
	if poll.Voters[voterID] {
		return fmt.Errorf("voter %s has already voted", voterID)
	}

	// Check if the candidate exists in the poll
	if _, exists := poll.Votes[candidate]; !exists {
		return fmt.Errorf("candidate %s does not exist in poll", candidate)
	}

	// Cast the vote
	poll.Votes[candidate]++
	poll.Voters[voterID] = true

	// Marshal and update the poll in state
	pollAsBytes, err = json.Marshal(poll)
	if err != nil {
		return fmt.Errorf("failed to marshal poll: %v", err)
	}

	err = ctx.GetStub().PutState(pollID, pollAsBytes)
	if err != nil {
		return fmt.Errorf("failed to update poll in world state: %v", err)
	}

	// Emitting an event
	err = ctx.GetStub().SetEvent("CastVote", []byte(fmt.Sprintf("Voter %s cast vote for %s in poll %s", voterID, candidate, pollID)))
	if err != nil {
		return fmt.Errorf("failed to set event for vote casting: %v", err)
	}

	return nil
}

// GetResults returns the current results of a poll
func (s *SmartContract) GetResults(ctx contractapi.TransactionContextInterface, pollID string) (map[string]int, error) {
	pollAsBytes, err := ctx.GetStub().GetState(pollID)
	if err != nil {
		return nil, fmt.Errorf("failed to read poll from world state: %v", err)
	}
	if pollAsBytes == nil {
		return nil, fmt.Errorf("poll %s does not exist", pollID)
	}

	var poll Poll
	err = json.Unmarshal(pollAsBytes, &poll)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal poll: %v", err)
	}

	return poll.Votes, nil
}
func (s *SmartContract) ListPolls(ctx contractapi.TransactionContextInterface) ([]Poll, error) {
	resultsIterator, err := ctx.GetStub().GetStateByRange("", "")
	if err != nil {
		return nil, fmt.Errorf("failed to get state by range: %v", err)
	}
	defer resultsIterator.Close()

	var polls []Poll
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return nil, fmt.Errorf("failed to get next result: %v", err)
		}

		var poll Poll
		err = json.Unmarshal(queryResponse.Value, &poll)
		if err != nil {
			return nil, fmt.Errorf("failed to unmarshal poll: %v", err)
		}

		// Validate that fields are properly initialized
		if poll.Candidates == nil {
			poll.Candidates = []string{}
		}
		if poll.Votes == nil {
			poll.Votes = make(map[string]int)
		}
		if poll.Voters == nil {
			poll.Voters = make(map[string]bool)
		}

		polls = append(polls, poll)
	}

	return polls, nil
}
