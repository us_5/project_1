package main

import (
	"log"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
	// Import other necessary packages if any
)

func main() {
	chaincode, err := contractapi.NewChaincode(new(SmartContract))
	if err != nil {
		log.Fatalf("Error creating chaincode: %v", err)
	}

	if err := chaincode.Start(); err != nil {
		log.Fatalf("Error starting chaincode: %v", err)
	}
}
