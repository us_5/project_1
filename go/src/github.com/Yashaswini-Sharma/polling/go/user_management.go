package main

import (
	"crypto/rand"
	"encoding/json"
	"fmt"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

type SmartContract struct {
	contractapi.Contract
}

type User struct {
	ID       string `json:"id"`
	Username string `json:"username"`
	Password string `json:"password"`
}

type Session struct {
	UserID string `json:"userId"`
}

// GetUserNameFromUserID retrieves the username based on the user ID.
func (s *SmartContract) GetUserNameFromUserID(ctx contractapi.TransactionContextInterface, userID string) (string, error) {
	userAsBytes, err := ctx.GetStub().GetState(userID)
	if err != nil {
		return "", fmt.Errorf("failed to read user: %v", err)
	}
	if userAsBytes == nil {
		return "", fmt.Errorf("user not found")
	}

	var user User
	err = json.Unmarshal(userAsBytes, &user)
	if err != nil {
		return "", fmt.Errorf("failed to unmarshal user: %v", err)
	}

	return user.Username, nil
}

// CreateUser creates a new user and stores it in the ledger.
func (s *SmartContract) CreateUser(ctx contractapi.TransactionContextInterface, username string, password string) error {
	userID := generateID("USER")
	user := User{
		ID:       userID,
		Username: username,
		Password: password,
	}

	userAsBytes, err := json.Marshal(user)
	if err != nil {
		return fmt.Errorf("failed to marshal user: %v", err)
	}

	return ctx.GetStub().PutState(userID, userAsBytes)
}

func (s *SmartContract) AuthenticateUser(ctx contractapi.TransactionContextInterface, username string, password string) (string, error) {
	queryString := fmt.Sprintf("{\"selector\":{\"username\":\"%s\",\"password\":\"%s\"}}", username, password)
	resultsIterator, err := ctx.GetStub().GetQueryResult(queryString)
	if err != nil {
		return "", fmt.Errorf("error querying user: %v", err)
	}
	defer resultsIterator.Close()

	if resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return "", fmt.Errorf("error iterating query results: %v", err)
		}

		var user User
		err = json.Unmarshal(queryResponse.Value, &user)
		if err != nil {
			return "", fmt.Errorf("error unmarshalling user data: %v", err)
		}

		return user.ID, nil // Return only the userID as a plain string
	}

	return "", nil
}

// LogoutUser deletes the current session.
func (s *SmartContract) LogoutUser(ctx contractapi.TransactionContextInterface) error {
	err := ctx.GetStub().DelState("currentSession")
	if err != nil {
		return fmt.Errorf("error deleting session: %v", err)
	}

	return nil
}

// GetCurrentUser retrieves the current logged-in user ID from the session.
func (s *SmartContract) GetCurrentUser(ctx contractapi.TransactionContextInterface) (string, error) {
	sessionKey := "currentSession"
	sessionAsBytes, err := ctx.GetStub().GetState(sessionKey)
	if err != nil {
		return "", fmt.Errorf("error retrieving session data: %v", err)
	}
	if sessionAsBytes == nil {
		return "", fmt.Errorf("no user currently logged in")
	}

	var session Session
	err = json.Unmarshal(sessionAsBytes, &session)
	if err != nil {
		return "", fmt.Errorf("error unmarshalling session data: %v", err)
	}

	return session.UserID, nil
}

// generateID generates a unique ID with the given prefix.
func generateID(prefix string) string {
	randomBytes := make([]byte, 4)
	_, err := rand.Read(randomBytes)
	if err != nil {
		panic(fmt.Errorf("failed to generate random bytes: %v", err))
	}

	id := fmt.Sprintf("%s-%x", prefix, randomBytes)
	return id
}
