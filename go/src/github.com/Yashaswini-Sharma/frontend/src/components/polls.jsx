import React, { useState } from 'react';
import axios from 'axios';

function Polls() {
    const [title, setTitle] = useState('');
    const [candidates, setCandidates] = useState('');
    const [pollCreationResult, setPollCreationResult] = useState('');
    const [pollResults, setPollResults] = useState({});
    const [pollID, setPollID] = useState('');

    const handleCreatePoll = async (e) => {
        e.preventDefault();
        const candidateList = candidates.split(',').map(candidate => candidate.trim());
        try {
            const response = await axios.post('http://localhost:5000/createPoll', {
                title,
                candidates: candidateList
            });
            if (response.status === 200) {
                setPollID(response.data.pollID);
                setPollCreationResult('Poll created successfully. Poll ID: ' + response.data.pollID);
            } else {
                setPollCreationResult('Failed to create poll');
            }
        } catch (error) {
            setPollCreationResult(`Failed to create poll: ${error.message}`);
        }
    };

    const handleGetResults = async () => {
        try {
            const response = await axios.get(`http://localhost:5000/getPollResults/${pollID}`);
            setPollResults(response.data);
        } catch (error) {
            setPollResults({ error: `Failed to get poll results: ${error.message}` });
        }
    };

    return (
        <div className="container">
            <h2>Create Poll</h2>
            <form onSubmit={handleCreatePoll}>
                <div className="form-group">
                    <label htmlFor="title">Poll Title</label>
                    <input
                        type="text"
                        id="title"
                        value={title}
                        onChange={(e) => setTitle(e.target.value)}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="candidates">Candidates (comma separated)</label>
                    <input
                        type="text"
                        id="candidates"
                        value={candidates}
                        onChange={(e) => setCandidates(e.target.value)}
                    />
                </div>
                <button type="submit">Create Poll</button>
            </form>
            {pollCreationResult && (
                <div className={`alert ${pollCreationResult.includes('successfully') ? 'alert-success' : 'alert-danger'}`}>
                    {pollCreationResult}
                </div>
            )}

            <h2>Get Poll Results</h2>
            <div className="form-group">
                <label htmlFor="pollID">Poll ID</label>
                <input
                    type="text"
                    id="pollID"
                    value={pollID}
                    onChange={(e) => setPollID(e.target.value)}
                />
            </div>
            <button onClick={handleGetResults}>Get Results</button>
            {Object.keys(pollResults).length > 0 && (
                <div className="results">
                    <h3>Poll Results</h3>
                    {pollResults.error ? (
                        <div className="alert alert-danger">{pollResults.error}</div>
                    ) : (
                        <ul>
                            {Object.entries(pollResults).map(([candidate, votes]) => (
                                <li key={candidate}>{candidate}: {votes} votes</li>
                            ))}
                        </ul>
                    )}
                </div>
            )}
        </div>
    );
}

export default Polls;
