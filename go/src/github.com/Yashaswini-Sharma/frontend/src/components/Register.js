import React, { useState } from 'react';
import axios from 'axios';
import './Register.css';

function Register() {
    const [id, setId] = useState('');
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [loading, setLoading] = useState(false);
    const [successMessage, setSuccessMessage] = useState('');
    const [errorMessage, setErrorMessage] = useState('');

    const handleSubmit = async (e) => {
        e.preventDefault();
        setLoading(true);
        setSuccessMessage('');
        setErrorMessage('');
        try {
            await axios.post('http://localhost:5000/createUser', {
                username,
                password
            }, {
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            setSuccessMessage('User registered successfully');
        } catch (error) {
            setErrorMessage(`Failed to register user: ${error.message}`);
        } finally {
            setLoading(false);
        }
    };

    return (
        <div className="container">
            <h3>Register</h3>
            <form onSubmit={handleSubmit}>
                <div className="form-group">

                    <label htmlFor="username">Username</label>
                    <input
                        type="text"
                        id="username"
                        value={username}
                        onChange={(e) => setUsername(e.target.value)}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="password">Password</label>
                    <input
                        type="password"
                        id="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                    />
                </div>
                <button type="submit" disabled={loading}>
                    {loading ? 'Registering...' : 'Register'}
                </button>
            </form>
            {successMessage && (
                <div className="alert alert-success">
                    {successMessage}
                </div>
            )}
            {errorMessage && (
                <div className="alert alert-danger">
                    {errorMessage}
                </div>
            )}
        </div>
    );
}

export default Register;

