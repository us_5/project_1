import React, { useState, useEffect } from 'react';
import axios from 'axios';
import './Authentication.css';

function Authentication() {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [authResult, setAuthResult] = useState('');
    const [loading, setLoading] = useState(false);
    const [currentUser, setCurrentUser] = useState(null);

    useEffect(() => {
        fetchCurrentUser();
    }, []);

    const handleSubmit = async (e) => {
        e.preventDefault();
        setLoading(true);
        setAuthResult('');
        try {
            const response = await axios.post('http://localhost:5000/authenticateUser', {
                username,
                password
            }, {
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            console.log("Authentication response:", response.data); // Debug log
            if (response.data.success) {
                setAuthResult(`Authentication successful: ${response.data.message}`);
                setCurrentUser(response.data.userID);
                fetchCurrentUser(); // Update current user after successful login
            } else {
                setAuthResult(`Authentication failed: ${response.data.message}`);
            }
        } catch (error) {
            console.error(`Failed to authenticate user: ${error.response?.data?.message || error.message}`);
            setAuthResult(`Failed to authenticate user: ${error.response?.data?.message || error.message}`);
        } finally {
            setLoading(false);
        }
    };

    const handleLogout = async () => {
        setLoading(true);
        try {
            const response = await axios.post('http://localhost:5000/logoutUser', {}, {
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            console.log("Logout response:", response.data); // Debug log
            setAuthResult('Logged out successfully');
            setCurrentUser(null);
            fetchCurrentUser(); // Update current user after logout
        } catch (error) {
            console.error(`Failed to logout user: ${error.response?.data?.message || error.message}`);
            setAuthResult(`Failed to logout user: ${error.response?.data?.message || error.message}`);
        } finally {
            setLoading(false);
        }
    };

    const fetchCurrentUser = async () => {
        try {
            const response = await axios.post('http://localhost:5000/getCurrentUser', {}, {
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            console.log("Current user response:", response.data); // Debug log
            setCurrentUser(response.data.userID);
        } catch (error) {
            console.error(`Failed to fetch current user: ${error.response?.data?.message || error.message}`);
        }
    };

    useEffect(() => {
        console.log("Current user updated:", currentUser); // Debug log
    }, [currentUser]);

    return (
        <div className="container">
            {currentUser ? (
                <div>
                    <h2>Logged in as: {currentUser}</h2>
                    <button onClick={handleLogout} disabled={loading}>
                        {loading ? 'Logging out...' : 'Logout'}
                    </button>
                </div>
            ) : (
                <div>
                    <h2>Authenticate User</h2>
                    <form onSubmit={handleSubmit}>
                        <div className="form-group">
                            <label htmlFor="username">Username</label>
                            <input
                                type="text"
                                id="username"
                                value={username}
                                onChange={(e) => setUsername(e.target.value)}
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">Password</label>
                            <input
                                type="password"
                                id="password"
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                            />
                        </div>
                        <button type="submit" disabled={loading}>
                            {loading ? 'Authenticating...' : 'Authenticate'}
                        </button>
                    </form>
                </div>
            )}
            {authResult && (
                <div className={`alert ${authResult.includes('successful') ? 'alert-success' : 'alert-danger'}`}>
                    {authResult}
                </div>
            )}
        </div>
    );
}

export default Authentication;
