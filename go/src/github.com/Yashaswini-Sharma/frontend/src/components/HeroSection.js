import React from 'react';
import '../App.css';
import { Button } from './button';
import './HeroSection.css';
import NavBar from './navbar';
import { Link } from 'react-router-dom';

function HeroSection() {
  return (
    <div className='hero-container'>
      <video src='/videos/video-2.mp4' autoPlay loop muted />
      <h1>YOUR CHOICE, YOUR VOTE</h1>
      <p>What are you waiting for?</p>
      <div className='hero-btns'>
        {/* <Button className='btns' buttonStyle='btn--outline' buttonSize='btn--large'
        >
          GET STARTED
        </Button> */}
        <Button
          className='btns' buttonStyle='btn--outline' buttonSize='btn--large'
        >
          WATCH TUTORIAL <i className='far fa-play-circle' />
        </Button>
        <Button
          className='btns' buttonStyle='btn--primary' buttonSize='btn--large'
        >
          GET STARTED
          <i className='far fa-play-circle' />
        </Button>
        {/* <Link to="https://github.com/briancodex/react-website-v1/blob/fd9baf8ac070b1c65c7f8a646ca9fa6ac59c89d9/public/videos/video-2.mp4"><button >Link to the video</button></Link> */}


      </div>

    </div>
  );

}
export default HeroSection;
