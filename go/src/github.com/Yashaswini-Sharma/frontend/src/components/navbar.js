import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import { Button } from './button';
import './navbar.css';
const NavBar = () => {
  const [click, setClick] = useState(false);
  const [button, setButton] = useState(true);

  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);

  const showButton = () => {
    if (window.innerWidth <= 960) {
      setButton(false);
    } else{
      setButton(true);
    }
  };

  useEffect(() => {
    showButton();
  }, []);

  window.addEventListener('resize', showButton);

  return (
    <>
      <nav className="navbar">
          <div className="navbar-container">
            <Link to="/" className="navbar-logo" onClick= {closeMobileMenu}>
                NIRNAYA <i className='fab fa-typo3'/>
            </Link>
            <div className='menu-icon' onClick={handleClick}>
              <i className={click ? 'fas fa-times' : 'fas fa-bars'}>{click ? "x" : "≡"}</i>
            </div>
            <ul className={click ? 'nav-menu active' : 'nav-menu'} style={{backgroundColor: 'black'}}>
              <li className='nav-item'>
                <Link to='/' className='nav-links' onClick={closeMobileMenu}>
                Home
                </Link>

              </li>
              <li className='nav-item'>
                <Link to='/about' className='nav-links' onClick={closeMobileMenu}>
                About
                </Link>

              </li>
              <li className='nav-item'>
                <Link to='/help' className='nav-links' onClick={closeMobileMenu}>
                Help
                </Link>

              </li>
              <li className='nav-item'>
                <Link to='/sign-up' className='nav-links-mobile' onClick={closeMobileMenu}>
                Sign Up
                </Link>

              </li>

            </ul>
            {button && <Button buttonStyle='btn--outline'>SIGN UP</Button>}
          </div>
        </nav>
    </>
  );
}

export default NavBar;


