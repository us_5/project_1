import React, { useEffect, useState } from 'react';
import axios from 'axios';

const Vote = () => {
    const [polls, setPolls] = useState([]);
    const [message, setMessage] = useState('');

    useEffect(() => {
        fetchPolls();
    }, []);
    

    const fetchPolls = async () => {
        try {
            const response = await axios.get('http://localhost:5000/listPolls');
            const response1 = await axios.post('http://localhost:5000/getCurrentUser', {}, {
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            console.log(response1)
            setPolls(response.data);
        } catch (error) {
            console.error('Failed to fetch polls:', error);
        }
    };

    const handleVote = async (pollID, candidate) => {
        try {
            const response = await axios.post('http://localhost:5000/castVote', { pollID, candidate });
            setMessage(response.data);
        } catch (error) {
            console.error('Failed to cast vote:', error);
            setMessage('Failed to cast vote');
        }
    };

    return (
        <div>
            <h1>Polls</h1>
            {polls.length === 0 ? (
                <p>No polls available</p>
            ) : (
                polls.map(poll => (
                    <div key={poll.pollID}>
                        <h2>{poll.title}</h2>
                        {poll.candidates.map(candidate => (
                            <button key={candidate} onClick={() => handleVote(poll.pollID, candidate)}>
                                {candidate}
                            </button>
                        ))}
                    </div>
                ))
            )}
            {message && <p>{message}</p>}
        </div>
    );
};

export default Vote;
