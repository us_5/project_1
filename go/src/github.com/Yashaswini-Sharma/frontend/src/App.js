// src/App.js
import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Register from './components/Register';
import Authentication from './components/Authenticate';
import Home from './components/Home';
import About from './components/About';
import Help from './components/Help';
import SignUp from './components/SignUp';
import NavBar from './components/navbar';
import Polls from './components/polls';
import axios from 'axios';
import Vote from './components/vote';

function App() {
  const [loggedInUser, setLoggedInUser] = useState(null);

  useEffect(() => {
    const fetchCurrentUser = async () => {
      try {
        const response = await axios.post('http://localhost:5000/getCurrentUser');
        setLoggedInUser(response.data.userID);
      } catch (error) {
        console.log('No user logged in');
      }
    };
    fetchCurrentUser();
  }, []);

  return (
    <Router>

      <div className="App">
        <Routes>
          <Route path='/' exact element={<Home />} />
          <Route path='/about' element={<About />} />
          <Route path='/help' element={<Help />} />
          <Route path='/sign-up' element={<SignUp />} />
          <Route path="/register" element={<Register />} />
          <Route path="/login" element={<Authentication setLoggedInUser={setLoggedInUser} />} />
          <Route path="/polls" element={<Polls loggedInUser={loggedInUser} setLoggedInUser={setLoggedInUser} />} />
          <Route path="/vote" element={<Vote />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
