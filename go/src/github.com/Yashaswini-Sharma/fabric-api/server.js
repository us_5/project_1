const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const session = require('express-session');
const { Gateway, Wallets } = require('fabric-network');
const path = require('path');
const fs = require('fs');
const yaml = require('js-yaml');

const app = express();
app.use(bodyParser.json());
app.use(cors());

app.use(session({
    secret: 'your_secret_key',
    resave: false,
    saveUninitialized: false, // Change to false to avoid saving uninitialized sessions
    cookie: { secure: false }
}));
const ccpPath = '/home/naina/Projects/project_1/go/src/github.com/Yashaswini-Sharma/fabric-samples/test-network/config.yaml';

// Helper function to get the contract
async function getContract() {
    const walletPath = path.join(process.cwd(), 'wallet');
    const wallet = await Wallets.newFileSystemWallet(walletPath);
    const gateway = new Gateway();
    const ccp = yaml.load(fs.readFileSync(ccpPath, 'utf8'));
    await gateway.connect(ccp, { wallet, identity: 'appUser', discovery: { enabled: true, asLocalhost: true } });
    const network = await gateway.getNetwork('mychannel');
    const contract = network.getContract('marbles'); // Use the correct chaincode name
    return { contract, gateway };
}

app.post('/createUser', async (req, res) => {
    const { username, password } = req.body;

    try {
        const { contract, gateway } = await getContract();
        await contract.submitTransaction('CreateUser', username, password);
        await gateway.disconnect();
        res.status(200).send('User created successfully');
    } catch (error) {
        console.error(`Failed to create user: ${error}`);
        res.status(500).send(`Failed to create user: ${error.message}`);
    }
});

app.post('/authenticateUser', async (req, res) => {
    const { username, password } = req.body;

    try {
        const { contract, gateway } = await getContract();
        const result = await contract.evaluateTransaction('AuthenticateUser', username, password);
        const authenticationResult = JSON.parse(result.toString());

        if (authenticationResult) {
            req.session.user = username; // Store user in session
            res.status(200).json({ success: true, message: 'User authenticated successfully', userID: username });
        } else {
            res.status(200).json({ success: false, message: 'Invalid username or password' });
        }

        await gateway.disconnect();
    } catch (error) {
        console.error(`Failed to authenticate user: ${error}`);
        res.status(500).send(`Failed to authenticate user: ${error.message}`);
    }
});

app.post('/getCurrentUser', async (req, res) => {
    try {
        if (req.session.user) {
            res.status(200).json({ userID: req.session.user });
        } else {
            res.status(400).send('No user logged in');
        }
    } catch (error) {
        console.error(`Failed to get current user: ${error}`);
        res.status(500).send(`Failed to get current user: ${error.message}`);
    }
});
app.post('/castVote', async (req, res) => {
    const { pollID, candidate } = req.body;

    console.log(`Received castVote request with pollID: ${pollID}, candidate: ${candidate}`);

    if (!pollID || !candidate) {
        return res.status(400).send('Invalid input: pollID and candidate are required');
    }

    if (!req.session.user) {
        return res.status(401).send('User not authenticated');
    }

    try {
        const { contract, gateway } = await getContract();
        const user = req.session.user;
        console.log(`User ${user} is casting vote for pollID: ${pollID}, candidate: ${candidate}`);
        await contract.submitTransaction('CastVote', pollID, candidate);
        await gateway.disconnect();
        res.status(200).send('Vote cast successfully');
    } catch (error) {
        console.error(`Failed to cast vote: ${error}`);
        res.status(500).send(`Failed to cast vote: ${error.message}`);
    }
});


app.post('/logoutUser', async (req, res) => {
    try {
        req.session.destroy(); // Destroy the session
        res.status(200).send('User logged out successfully');
    } catch (error) {
        console.error(`Failed to logout user: ${error}`);
        res.status(500).send(`Failed to logout user: ${error.message}`);
    }
});


app.get('/listPolls', async (req, res) => {
    try {
        const { contract, gateway } = await getContract();
        const result = await contract.evaluateTransaction('ListPolls');
        const polls = JSON.parse(result.toString());
        await gateway.disconnect();
        res.status(200).json(polls);
    } catch (error) {
        console.error(`Failed to list polls: ${error}`);
        res.status(500).send(`Failed to list polls: ${error.message}`);
    }
});

app.post('/createPoll', async (req, res) => {
    const { title, candidates } = req.body;

    try {
        const { contract, gateway } = await getContract();
        const result = await contract.submitTransaction('CreatePoll', title, JSON.stringify(candidates));
        const pollID = result.toString();
        await gateway.disconnect();
        res.status(200).json({ pollID });
    } catch (error) {
        console.error(`Failed to create poll: ${error}`);
        res.status(500).send(`Failed to create poll: ${error.message}`);
    }
});

app.get('/getPollResults/:pollID', async (req, res) => {
    const { pollID } = req.params;

    try {
        const { contract, gateway } = await getContract();
        const result = await contract.evaluateTransaction('GetResults', pollID);
        const results = JSON.parse(result.toString());
        await gateway.disconnect();
        res.status(200).json(results);
    } catch (error) {
        console.error(`Failed to get poll results: ${error}`);
        res.status(500).send(`Failed to get poll results: ${error.message}`);
    }
});

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
