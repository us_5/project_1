package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/hyperledger/fabric-sdk-go/pkg/client/channel"
	"github.com/hyperledger/fabric-sdk-go/pkg/core/config"
	"github.com/hyperledger/fabric-sdk-go/pkg/fabsdk"
)

const (
	configPath  = "/home/naina/Projects/project_1/go/src/github.com/Yashaswini-Sharma/fabric-samples/test-network/config.yaml"
	channelID   = "mychannel"
	chaincodeID = "marbles"
)

// CreateUserHandler handles user creation requests
func createUserHandler(w http.ResponseWriter, r *http.Request) {
	var user struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	sdk, err := fabsdk.New(config.FromFile(configPath))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer sdk.Close()

	channelContext := sdk.ChannelContext(channelID, fabsdk.WithUser("Admin"))
	client, err := channel.New(channelContext)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Convert []string to [][]byte
	args := [][]byte{
		[]byte(user.Username),
		[]byte(user.Password),
	}

	_, err = client.Execute(channel.Request{
		ChaincodeID: chaincodeID,
		Fcn:         "CreateUser",
		Args:        args,
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)
}

// AuthenticateUserHandler handles user authentication requests
func authenticateUserHandler(w http.ResponseWriter, r *http.Request) {
	var user struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	sdk, err := fabsdk.New(config.FromFile(configPath))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer sdk.Close()

	channelContext := sdk.ChannelContext(channelID, fabsdk.WithUser("Admin"))
	client, err := channel.New(channelContext)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Convert []string to [][]byte
	args := [][]byte{
		[]byte(user.Username),
		[]byte(user.Password),
	}

	result, err := client.Query(channel.Request{
		ChaincodeID: chaincodeID,
		Fcn:         "AuthenticateUser",
		Args:        args,
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if string(result.Payload) == "true" {
		w.WriteHeader(http.StatusOK)
	} else {
		http.Error(w, "Invalid credentials", http.StatusUnauthorized)
	}
}

func main() {
	http.HandleFunc("/create-user", createUserHandler)
	http.HandleFunc("/authenticate-user", authenticateUserHandler)

	log.Fatal(http.ListenAndServe(":8080", nil))
}
