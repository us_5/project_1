package main

import (
	"encoding/json"
	"fmt"
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

// SmartContract provides functions for managing votes
type SmartContract struct {
	contractapi.Contract
}

// Poll describes basic details of a poll
type Poll struct {
	ID        string   `json:"id"`
	Title     string   `json:"title"`
	Candidates []string `json:"candidates"`
	Votes     map[string]int `json:"votes"`
	Voters    map[string]bool `json:"voters"`
}

// CreatePoll initializes a new poll
func (s *SmartContract) CreatePoll(ctx contractapi.TransactionContextInterface, pollID string, title string, candidates []string) error {
	poll := Poll{
		ID:        pollID,
		Title:     title,
		Candidates: candidates,
		Votes:     make(map[string]int),
		Voters:    make(map[string]bool),
	}

	for _, candidate := candidates {
		poll.Votes[candidate] = 0
	}

	pollAsBytes, _ := json.Marshal(poll)
	return ctx.GetStub().PutState(pollID, pollAsBytes)
}

// CastVote allows a user to cast a vote
func (s *SmartContract) CastVote(ctx contractapi.TransactionContextInterface, pollID string, voterID string, candidate string) error {
	pollAsBytes, err := ctx.GetStub().GetState(pollID)
	if err != nil {
		return fmt.Errorf("failed to read poll from world state: %v", err)
	}
	if pollAsBytes == nil {
		return fmt.Errorf("poll %s does not exist", pollID)
	}

	var poll Poll
	err = json.Unmarshal(pollAsBytes, &poll)
	if err != nil {
		return fmt.Errorf("failed to unmarshal poll: %v", err)
	}

	if poll.Voters[voterID] {
		return fmt.Errorf("voter %s has already voted", voterID)
	}

	if _, exists := poll.Votes[candidate]; !exists {
		return fmt.Errorf("candidate %s does not exist in poll", candidate)
	}

	poll.Votes[candidate]++
	poll.Voters[voterID] = true

	pollAsBytes, err = json.Marshal(poll)
	if err != nil {
		return fmt.Errorf("failed to marshal poll: %v", err)
	}

	return ctx.GetStub().PutState(pollID, pollAsBytes)
}

// GetResults returns the current results of a poll
func (s *SmartContract) GetResults(ctx contractapi.TransactionContextInterface, pollID string) (map[string]int, error) {
	pollAsBytes, err := ctx.GetStub().GetState(pollID)
	if err != nil {
		return nil, fmt.Errorf("failed to read poll from world state: %v", err)
	}
	if pollAsBytes == nil {
		return nil, fmt.Errorf("poll %s does not exist", pollID)
	}

	var poll Poll
	err = json.Unmarshal(pollAsBytes, &poll)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal poll: %v", err)
	}

	return poll.Votes, nil
}

func main() {
	chaincode, err := contractapi.NewChaincode(new(SmartContract))
	if err != nil {
		fmt.Printf("Error create voting chaincode: %s", err.Error())
		return
	}

	if err := chaincode.Start(); err != nil {
		fmt.Printf("Error starting voting chaincode: %s", err.Error())
	}
}

