after pulling the voting_chaincode.go file the follwoing steps need to be done:-

1)Package the Chaincode
tar -czf voting.tar.gz *

2)Install the chaincode
peer lifecycle chaincode install voting.tar.gz

3)Approve the Chaincode for Your Organization
Approve the chaincode for your organization. Make sure to replace <sequence>, <channel-name>, <cc-name>, <version>, <init-required>, and <package-id> with your actual values.

command to execute for the same is:-
peer lifecycle chaincode approveformyorg -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --channelID mychannel --name votingcc --version 1.0 --package-id <package-id> --sequence 1 --tls --cafile $ORDERER_CA

4)Commit the Chaincode
peer lifecycle chaincode commit -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --channelID mychannel --name votingcc --version 1.0 --sequence 1 --tls --cafile $ORDERER_CA --peerAddresses localhost:7051 --tlsRootCertFiles $PEER0_ORG1_CA

5)Invoke the Chaincode
 i) Create a Poll
    peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls --cafile $ORDERER_CA -C mychannel -n votingcc --peerAddresses localhost:7051 --tlsRootCertFiles $PEER0_ORG1_CA -c '{"function":"CreatePoll","Args":["poll1", "Election 2024", "Alice", "Bob"]}'
 
 ii) Cast a Vote
     peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls --cafile $ORDERER_CA -C mychannel -n votingcc --peerAddresses localhost:7051 --tlsRootCertFiles $PEER0_ORG1_CA -c '{"function":"CastVote","Args":["poll1", "user1", "Alice"]}'

 iii) Get Results
      peer chaincode query -C mychannel -n votingcc -c '{"function":"GetResults","Args":["poll1"]}'
