package main

import (
	"fmt"
	"io/ioutil"
	"log"

	"github.com/hyperledger/fabric-sdk-go/pkg/client/resmgmt"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/errors/retry"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/providers/context"
	"github.com/hyperledger/fabric-sdk-go/pkg/core/config"
	"github.com/hyperledger/fabric-sdk-go/pkg/fabsdk"
)

func main() {
	configPath := "/home/naina/Projects/project_1/go/src/github.com/Yashaswini-Sharma/fabric-samples/test-network/config.yaml"

	// Print the configuration file content
	configContent, err := ioutil.ReadFile(configPath)
	if err != nil {
		log.Fatalf("Failed to read config file: %s", err)
	}
	fmt.Printf("Config file content:\n%s\n", string(configContent))

	// Load the SDK config
	sdk, err := fabsdk.New(config.FromFile(configPath))
	if err != nil {
		log.Fatalf("Failed to create new SDK: %s", err)
	}
	defer sdk.Close()

	// Helper function to create a resource management client for an org
	createResMgmtClient := func(sdk *fabsdk.FabricSDK, orgName string, userName string) (*resmgmt.Client, context.ClientProvider, error) {
		log.Printf("Creating client context for Org: %s, User: %s", orgName, userName)
		clientContext := sdk.Context(fabsdk.WithUser(userName), fabsdk.WithOrg(orgName))
		resMgmtClient, err := resmgmt.New(clientContext)
		if err != nil {
			log.Printf("Error creating resource management client for Org: %s, User: %s - %s", orgName, userName, err)
			return nil, nil, err
		}
		return resMgmtClient, clientContext, nil
	}

	// Create a resource management client for Org1
	resMgmtClientOrg1, _, err := createResMgmtClient(sdk, "Org1", "Admin")
	if err != nil {
		log.Fatalf("Failed to create resource management client for Org1: %s", err)
	}

	// Create a resource management client for Org2
	resMgmtClientOrg2, _, err := createResMgmtClient(sdk, "Org2", "Admin")
	if err != nil {
		log.Fatalf("Failed to create resource management client for Org2: %s", err)
	}

	// Read the packaged chaincode
	ccPackagePath := "/home/naina/Projects/project_1/go/src/github.com/Yashaswini-Sharma/fabric-samples/test-network/authcc.tar.gz"
	ccPkg, err := ioutil.ReadFile(ccPackagePath)
	if err != nil {
		log.Fatalf("Failed to read chaincode package: %s", err)
	}

	// Install chaincode on peer0.org1
	installCCReq := resmgmt.LifecycleInstallCCRequest{
		Label:   "authcc_1.0",
		Package: ccPkg,
	}
	_, err = resMgmtClientOrg1.LifecycleInstallCC(installCCReq, resmgmt.WithRetry(retry.DefaultResMgmtOpts), resmgmt.WithTargetEndpoints("peer0.org1.example.com"))
	if err != nil {
		log.Fatalf("Failed to install chaincode on peer0.org1: %s", err)
	}

	// Install chaincode on peer0.org2
	_, err = resMgmtClientOrg2.LifecycleInstallCC(installCCReq, resmgmt.WithRetry(retry.DefaultResMgmtOpts), resmgmt.WithTargetEndpoints("peer0.org2.example.com"))
	if err != nil {
		log.Fatalf("Failed to install chaincode on peer0.org2: %s", err)
	}

	log.Println("Chaincode 'authcc' installed successfully on both peers.")
}
