# NIRNAYA

NIRNAYA is an online voting system that leverages the power of GO and Blockchain Technology to provide a secure, transparent, and efficient platform for creating and participating in polls.

## **TEAM MEMBERS** 

1. Adyasha Das
2. Bipasa Maji
3. Lithikha B
4. Sanjana S Pillai
5. Yashaswini Sharma

## **OBJECTIVE**

The main objective of this project is to gain practical experience and deepen our understanding of GO and Blockchain Technology, specifically utilizing Hyperledger Fabric.

## **FUNCTIONALITY**

Our application allows users to:
1. **Create Polls**: Users can create polls with various options for others to vote on. This feature is designed to be intuitive, making it easy for anyone to set up a poll.
2. **Cast Votes**: Once a poll is created, other users can cast their votes. The voting process is streamlined to ensure it is user-friendly and secure.

## **USER FLOW**

1. **Registration**: Users must first register on the application. This ensures that all participants are authenticated, which adds a layer of security and integrity to the voting process.
2. **Create a Poll**: After registering, users can create a poll by specifying the question and possible answers. The poll is then added to the list of active polls.
3. **Vote**: Registered users can browse active polls and cast their votes on the ones they are interested in. The voting process is straightforward, ensuring a smooth user experience.
4. **View Results**: The user who created the poll can view the results in real-time on the application. This feature provides immediate feedback and transparency.

## **TECH STACKS USED**

### *BACKEND*
1. **GO** - The backend logic of the application is built using GO for handling multiple users and transactions simultaneously.
2. **HYPERLEDGER FABRIC** -  To ensure the security and transparency of the voting process, we use Hyperledger Fabric, a permissioned blockchain framework.

### *FRONTEND*
1. **REACT** - The frontend of the application is developed using React, a popular JavaScript library

## **FILE STRUCTURE**

Here's a brief overview of our file structure

- **frontend/**
  - **public/**
  - **src/**
    - **assets/**
    - **components/**
    - **Pages/**
    - **styles/**
    - **App.js/**
    - **App.css/**
    - **index.js/**
    - **index.css/**
  - **package-lock.json/**
  - **package.json/**

- **go/**
  - **src/**
    - **github.com/Yashaswini-Sharma/**
      - **polling/go/**
        - **main.go/**
        - **user_management.go/**
        - **voting_management.go/**

- **discussions.txt**

- **README.md**




## REFERENCES

1. https://go.dev/doc/
2. https://hyperledger-fabric.readthedocs.io/en/release-2.5/
3. https://react.dev/learn
